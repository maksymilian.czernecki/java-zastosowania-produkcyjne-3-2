import java.math.BigDecimal;

public class Ticket {
    private BigDecimal price;
    private int customerAge;
    private int customerId;
    private int companyId;

    public Ticket(BigDecimal price, int customerAge, int customerId, int companyId) {
        this.price = price;
        this.customerAge = customerAge;
        this.customerId = customerId;
        this.companyId = companyId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getCustomerAge() {
        return customerAge;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "price=" + price +
                ", customerAge=" + customerAge +
                ", customerId=" + customerId +
                ", companyId=" + companyId +
                '}';
    }
}
