import org.apache.commons.cli.*;

import java.math.BigDecimal;

public class cliProcessor {
    static Ticket generateTicket(String[] args) {
        BigDecimal ticketPrice = BigDecimal.ZERO;
        int customerAge = 0, customerId = 0, companyId = 0;
        Options options = new Options();
        options.addOption("ticketPrice", true, "Ticket's price");
        options.addOption("customerAge", true, "Customer's age");
        options.addOption("customerId", true, "Customer's id");
        options.addOption("companyId", true, "Company's id");
        CommandLineParser commandLineParser = new DefaultParser();
        try {
            CommandLine commandLine = commandLineParser.parse(options, args);
            if (commandLine.hasOption("ticketPrice")) {
                ticketPrice = BigDecimal.valueOf(Double.valueOf(commandLine.getOptionValue("ticketPrice")));
            }
            if (commandLine.hasOption("customerAge")) {
                customerAge = Integer.valueOf(commandLine.getOptionValue("customerAge"));
            }
            if (commandLine.hasOption("customerId")) {
                customerId = Integer.valueOf(commandLine.getOptionValue("customerId"));
            }
            if (commandLine.hasOption("companyId")) {
                companyId = Integer.valueOf(commandLine.getOptionValue("companyId"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(!ticketPrice.multiply(BigDecimal.valueOf(customerAge*customerId*companyId*1.0)).equals(BigDecimal.ZERO)) {
            return new Ticket(ticketPrice, customerAge, customerId, companyId);
        } else {
            return null;
        }
    }
}
