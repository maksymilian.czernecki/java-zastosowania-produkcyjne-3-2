import com.external.PaymentsService;
import com.internal.DiscountCalculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

public class ticketGenerator {


    public static void main(String[] args) throws SomethingHappenedException {

        Logger logger = LoggerFactory.getLogger("TicketGenerator321");
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);
        Ticket ticket = cliProcessor.generateTicket(args);
        logger
                .debug(ticket
                        .toString());
        if (ticket != null) {
            ticket.setPrice(new DiscountCalculator().calculateDiscount(ticket.getPrice(), ticket.getCustomerAge()));
            logger.debug("Ticket price: " + ticket.getPrice());
            if (!(new PaymentsService().makePayment(ticket.getCustomerId(), ticket.getCompanyId(), ticket.getPrice()))) {
                throw new SomethingHappenedException();
            }

        }
    }

}

class SomethingHappenedException extends Exception {
    public SomethingHappenedException() {
    }
}
