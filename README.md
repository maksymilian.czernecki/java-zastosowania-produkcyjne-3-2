Use "java -jar loggingProject-1.0-SNAPSHOT.jar" to run. \
Options:\
    -ticketPrice [double] \
    -customerAge [int]\
    -customerId [int] \
    -companyId [int]

Due to inability to force log4j2 to cooperate with logback, they work together